struct Smartphone{
    var display: String
    var processor: String
    var ram: Int
    var storage : Int
    
    init(display : String, processor : String, ram : Int, storage : Int){
        self.ram = ram
        self.storage = storage
        self.processor = processor
        self.display = display
    }
    mutating func upgradeRam(to value :Int){
        ram = value
        print("Ram upgraded to \(value)GB")
    }
    
    mutating func upgradeStorage(to value :Int){
        storage = value
        print("Storage upgraded to \(value)GB")
    }
}
