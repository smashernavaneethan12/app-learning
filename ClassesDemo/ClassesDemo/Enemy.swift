class Enemy{
    var health : Int
    var attackStrength : Int
    
    init(){
        health = 0
        attackStrength = 0
    }
    init(health:Int,attackStrength:Int){
        self.health = health
        self.attackStrength = attackStrength
    }
    func move(){
        print("Walk Forward")
    }
    
    func attack(){
        print("Given \(attackStrength) damages.")
    }
    
}
