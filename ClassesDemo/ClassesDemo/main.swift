
var enemy1 = Enemy()
enemy1.move()
enemy1.attack()

var enemy:[Enemy] = []
let newEnemy = Enemy(health: 50, attackStrength: 30)
print(newEnemy.attackStrength)
enemy.append(newEnemy)
enemy.append(Enemy(health: 30, attackStrength: 10))

for i in enemy{
    i.attack()
    print(i.health)
}

let newPower = Powers()
newPower.attack()
newPower.health = 200
print(newPower.health)

var pocoF1 = Smartphone(display: "LCD", processor: "Snapdragon 845", ram: 8, storage: 128)

pocoF1.upgradeStorage(to: 256)
pocoF1.upgradeRam(to: 12)

var oneplus6 = pocoF1
oneplus6.upgradeRam(to: 8)

print(pocoF1.ram)
print(oneplus6.ram)
