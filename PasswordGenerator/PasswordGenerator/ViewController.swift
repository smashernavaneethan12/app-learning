//
//  ViewController.swift
//  PasswordGenerator
//
//  Created by Temp on 13/01/22.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var passwordLabel: UILabel!
    let letters =  ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordLabel.text = ""
        
    }

    @IBAction func generateButtonPressed(_ sender: UIButton) {
        
        passwordLabel.text = letters.randomElement()! + letters.randomElement()! + letters.randomElement()! + letters.randomElement()! + letters.randomElement()! + letters.randomElement()!
        
    }
    
}

