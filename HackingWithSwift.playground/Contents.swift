
var str = """
hello this is \
Hacking With Swift Playground file.
Thank You!!!
"""

print(str)


let a = 1

let bikeOwned = [
    "Ram" : "Mt - 15",
    "Ravi" : "NS 200",
    "Syed" : "Activa"
]
print(bikeOwned["Raju"] ?? "No Bike")
print(bikeOwned["Ram",default: "No Bike"])


let fibonacci = [1, 1, 2, 3, 5, 8, 13, 21]
var position = 0
while position <= 7 {
    let value = fibonacci[position]
    position += 1
    if value < 2 {
       continue
    }
    print("Fibonacci number \(position) is \(value)")
}

func changeString(input: inout String) -> String{
   input += "!"
    return input
}
var strN = "Hello"
print(changeString(input: &strN))
print(strN)


var string = "HelLLo"
string.lowercased()


var n: Int


let multiplyWithTwo = { (a:Int) -> Int in
    let c = a*2
    return c
}

print(multiplyWithTwo(2))

func multiplyAndAdd(a:Int,closure:(Int)->Int)->Int{
    let c = 2 + closure(a)
    return c
}

multiplyAndAdd(a: 2) { (a:Int)->Int in
    let c = a*2
    return c
}


var cutGrass = { (currentLength: Double) in
    switch currentLength {
    case 0...1:
        print("That's too short")
    case 1...3:
        print("It's already the right length")
    default:
        print("That's perfect.")
    }
}

cutGrass(1.1)

let end = 100_00.1
let range = 0...end
range.contains(1.1)

let printHello = {()-> String in
    return "Hello"
    
}
print(printHello())


var numArray = [23,45,12,456,134,234,1234,987]
var strArray = numArray.map{String($0)}
strArray = strArray.sorted()
print(strArray)
numArray = strArray.map{Int($0)!}
for num in numArray{
    print(num)
}


func swingBat() -> () -> Void {
    var strikes = 0
    return {
        strikes += 1
        if strikes >= 3 {
            print("You're out!")
        } else {
            print("Strike \(strikes)")
        }
    }
}
let swing = swingBat()
swing()
swing()
swing()
let swing2 = swingBat()
swing()
swing2()


func makeRandomNumberGenerator() -> () -> Int {
    var previousNumber = 0
    return {
       
        var newNumber: Int

        repeat {
            newNumber = Int.random(in: 1...3)
        } while newNumber == previousNumber

        previousNumber = newNumber
        return newNumber
    }
}
let generate = makeRandomNumberGenerator()
for _ in 1...10{
    print(generate())
}

struct Temp{
    init(){
        print("Temp")
    }
}


struct Student{
    var name:String
    static var studentsCount:Int = 0
    lazy var h = Temp()
    
    init(name:String){
        self.name = name
        print("Added \(name)")
        Student.studentsCount += 1
    }
    
    static func addId(id:Int){
        
    }
}

var s1 = Student(name: "Navaneethan")
s1.h
var s2 = Student(name: "Raju")

