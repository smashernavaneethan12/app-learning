//
//  BmiData.swift
//  learning
//
//  Created by Temp on 20/01/22.
//

import Foundation

struct BmiData{
    var height : Float
    var weight : Float
    init() {
        height = 0.0
        weight = 0.0
    }
    func getBmi() -> Float{
        return weight/(height * height)
    }
}
