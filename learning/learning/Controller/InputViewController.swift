//
//  ViewController.swift
//  learning
//
//  Created by Temp on 17/01/22.
//

import UIKit
extension Float {
    func rounded(toPlaces places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}

class InputViewController: UIViewController {

    @IBOutlet weak var heightSlider: UISlider!
    @IBOutlet weak var heightLabel: UILabel!
    
    @IBOutlet weak var weightSlider: UISlider!
    @IBOutlet weak var weightLabel: UILabel!
    
    var bmi = BmiData()
    override func viewDidLoad() {
        super.viewDidLoad()
        bmi.height = heightSlider.value.rounded(toPlaces: 2)
        bmi.weight = weightSlider.value.rounded(toPlaces: 2)
    }

    @IBAction func heightSliderPressed(_ sender: UISlider) {
        print(sender.value)
        bmi.height = sender.value.rounded(toPlaces: 2)
        heightLabel.text = "\(sender.value.rounded(toPlaces: 2)) M"
        
    }
    
    @IBAction func weightSliderPressed(_ sender: UISlider) {
        print(sender.value)
        bmi.weight = sender.value.rounded(toPlaces: 2)
        weightLabel.text = "\(sender.value.rounded(toPlaces: 2)) Kg"
    }
    
    @IBAction func calculateButtonPressed(_ sender: UIButton) {
        print(bmi.getBmi())
//        let result = ResultViewController()
//        result.bmi = bmi.getBmi()
        
        self.performSegue(withIdentifier: "goToResult", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToResult"{
            let destinationViewController = segue.destination as! ResultViewController
            destinationViewController.bmi = bmi.getBmi()
        }
    }
    
}

