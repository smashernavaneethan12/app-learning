//
//  ResultViewController.swift
//  learning
//
//  Created by Temp on 21/01/22.
//

import UIKit

class ResultViewController: UIViewController {
    
    
    @IBOutlet weak var resultLabel: UILabel!
    
    var bmi :Float = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        print(bmi)
        resultLabel.text = "Your BMI: \(bmi.rounded(toPlaces: 2))"
    }
    @IBAction func regeneratePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
