//
//  QuizBrain.swift
//  Quizzler-iOS13
//
//  Created by Temp on 18/01/22.
//  Copyright © 2022 The App Brewery. All rights reserved.
//

import Foundation
import UIKit

struct QuizBrain{
    var questions : [Quiz] = [
        Quiz(question: "How many Litres of Petrol did Vishnu and Vijayasekar stole from AS's Bike ?", answer1: "1",answer2: "2",answer3: "3",rightAnswer: "3"),
        Quiz(question: "When will Aamai gets committed ?", answer1: "1 Year",answer2: "2 Years",answer3: "Not possible",rightAnswer: "2 Years"),
        Quiz(question: "How many girlfriends do sekar have ?", answer1: "0",answer2: "1",answer3: "2",rightAnswer: "1"),
        Quiz(question: "When will Annachi say his love to his Uncle", answer1: "1 Year",answer2: "2 Year",answer3: "Never",rightAnswer: "Never"),
        Quiz(question: "When will we see Arjun in Big Screen ?", answer1: "1 Year",answer2: "2 Year",answer3: "Less Than 1 year",rightAnswer: "Less Than 1 year"),
        Quiz(question: "How many lives have been saved by Navaneethan as a Spider-Man ?", answer1: "2",answer2: "3",answer3: "Uncountabe",rightAnswer: "Uncountabe"),
        Quiz(question: "How many girls did vishnu cheated ?", answer1: "1 - 3", answer2: "3 - 5", answer3: "More than 5", rightAnswer: "More than 5")
    ]
    
    var questionNumber = 0
    var score = 0
    var colors: [UIColor] = [.systemRed,
                             .systemGreen,
                             .systemYellow,
                             .systemBlue,
                             .systemBrown
    ]
    
    mutating func checkAnswer(userAnswer answer:String)->Bool{
        if answer == questions[questionNumber].rightAnswer{
            score += 1
            return true
        } else{
            return false
        }
    }
    
    mutating func changeQuestionNumber() -> Bool{
        if questionNumber+1 < questions.count{
            questionNumber += 1
            return true
        }
        else{
//            questionNumber = 0
//            score = 0
            return false
        }
    }
    
    func getLabel() -> String{
        return questions[questionNumber].question
    }
    
    mutating func getScore() -> Int{
        return score
    }
    
    mutating func getProgress() -> Float{
        return Float(questionNumber)/Float(questions.count)
    }
    
    func getAnswer1() -> String{
        return questions[questionNumber].answer1
    }
    
    func getAnswer2() -> String{
        return questions[questionNumber].answer2
    }
    func getAnswer3() -> String{
        return questions[questionNumber].answer3
    }
    
}
