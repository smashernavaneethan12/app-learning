//
//  Quiz.swift
//  Quizzler-iOS13
//
//  Created by Temp on 18/01/22.
//  Copyright © 2022 The App Brewery. All rights reserved.
//

import Foundation

struct Quiz{
    var question = ""
    var answer1 = ""
    var answer2 = ""
    var answer3 = ""
    var rightAnswer = ""
}
