//
//  ViewController.swift
//  Quizzler-iOS13
//
//  Created by Angela Yu on 12/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var falseButton: UIButton!
    @IBOutlet weak var trueButton: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    var timer = Timer()
    var quizBrain = QuizBrain()
    var totalQuestion = 0
    
    func resetUI() {
        questionLabel.text = "Fininshed"
    }
    
    @objc func updateUI(){
        progressBar.progress = quizBrain.getProgress()
        questionLabel.text = quizBrain.getLabel()
        scoreLabel.text = "Score : \(quizBrain.getScore())"
        trueButton.backgroundColor = UIColor.clear
        falseButton.backgroundColor = UIColor.clear
        button3.backgroundColor = UIColor.clear
        trueButton.setTitle(quizBrain.getAnswer1(), for: .normal)
        falseButton.setTitle(quizBrain.getAnswer2(), for: .normal)
        button3.setTitle(quizBrain.getAnswer3(), for: .normal)
        trueButton.isHidden = false
        falseButton.isHidden = false
        button3.isHidden = false
        resetButton.isHidden = true
    }
    func reset(){
        questionLabel.text = "Finished !!!"
        scoreLabel.text = "Score : \(quizBrain.getScore())"
        falseButton.isHidden = true
        trueButton.isHidden = true
        button3.isHidden = true
        resetButton.isHidden = false
        //resetButton.backgroundColor = UIColor.clear
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        totalQuestion = quizBrain.questions.count
        let userAnswer = sender.currentTitle!
        if quizBrain.checkAnswer(userAnswer: userAnswer){
            print("Yes")
            sender.backgroundColor = UIColor.green
        }
        else{
            sender.backgroundColor = UIColor.red
            print("NO")
        }
        if quizBrain.changeQuestionNumber(){
            Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(updateUI), userInfo: nil, repeats: false)
        }else{
           reset()
        }
        
       
    }
    
    
    @IBAction func resetButtonPressed(_ sender: Any) {
        quizBrain.score = 0
        quizBrain.questionNumber = 0
        updateUI()
    }
    
}


