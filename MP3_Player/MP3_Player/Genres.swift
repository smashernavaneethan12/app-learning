//
//  Genres.swift
//  MP3_Player
//
//  Created by Temp on 22/01/22.
//

import Foundation

enum Genres{
    case melody
    case rock
    case pop
    case jazz
    case western
    case classic
}
