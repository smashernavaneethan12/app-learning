import Darwin
import Foundation

var signin = SignIn()

try signin.registerUser(newUserName: "Navaneethan F", userPassword: "123")


var mainTrack = AllTrack()
//mainTrack.playCurrentSong()
mainTrack.addNewSong(title: "Thuli Thuli", lengthInSeconds: 300, performedBy: ["Yuvan"], genre: Genres.melody, movie: "Paiya", writtenBy: ["Muthukumar"], producedBy: ["Yuvan"],time: 3)
mainTrack.addNewSong(title: "Poongatre", lengthInSeconds: 300, performedBy: ["Yuvan"], genre: Genres.classic, movie: "Paiya", writtenBy: ["Muthukumar"], producedBy: ["Yuvan"],time: 4)
mainTrack.addNewSong(title: "Adada Mazhaida", lengthInSeconds: 300, performedBy: ["Yuvan"], genre: Genres.pop, movie: "Paiya", writtenBy: ["Muthukumar"], producedBy: ["Yuvan"],time: 5)
mainTrack.addNewSong(title: "En Kadhal Solla", lengthInSeconds: 300, performedBy: ["Yuvan"], genre: Genres.western, movie: "Paiya", writtenBy: ["Muthukumar"], producedBy: ["Yuvan"],time: 2)
mainTrack.addNewSong(title: "Chellama", lengthInSeconds: 300, performedBy: ["Anirudh"], genre: Genres.western, movie: "Doctor", writtenBy: ["Muthukumar"], producedBy: ["Yuvan"],time: 2)
mainTrack.addNewSong(title: "Ambikapathy", lengthInSeconds: 300, performedBy: ["A R Rahman"], genre: Genres.western, movie: "Ambikapathy", writtenBy: ["Muthukumar"], producedBy: ["Yuvan"],time: 2)
//mainTrack.playCurrentSong()
//mainTrack.playPrev()
//mainTrack.playCurrentSong()

//mainTrack.showSongs(forAuthor: "Yuvan")
//
//do{
//    try mainTrack.playSong(songName: "Poongatree")
//}catch PlayerError.songError{
//    print(PlayerError.songError.rawValue)
//}
//
//mainTrack.showSongNumber()
//mainTrack.showDetails()
//mainTrack.showPlaylist()

var yuvanTrack = mainTrack.pushSongs(forArtist: "Yuvan")
//yuvanTrack.showPlaylist()
var anirudhTrack = mainTrack.pushSongs(forArtist: "Anirudh")
//anirudhTrack.showPlaylist()
var paiyaTrack = mainTrack.pushSongs(forMovie: "Paiya")
//paiyaTrack.showPlaylist()
//paiyaTrack.playSong(songNumber: 6)
mainTrack.sortSongs(by: "alphabatically")
mainTrack.sortSongs(by: "reversed")
mainTrack.showPlaylist()
mainTrack.playSong(songNumber: 2)
print(mainTrack.songnumber)
mainTrack.songnumber = 3
mainTrack.playCurrentSong()
mainTrack.sortSongs(by: "movie")
mainTrack.showPlaylist()
print(mainTrack.songnumber)

let newTrack = mainTrack
print(newTrack.totalSongs)
newTrack.removeSong(name: "En Kadhal Solla")
print(newTrack.totalSongs)
print(mainTrack.totalSongs)
