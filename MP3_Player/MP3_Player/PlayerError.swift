//
//  PlayerError.swift
//  MP3_Player
//
//  Created by Temp on 21/01/22.
//

import Foundation

enum PlayerError: String, Error{
    case songError = "No matching song found"
}
