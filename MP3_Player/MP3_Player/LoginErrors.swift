//
//  LoginErrors.swift
//  MP3_Player
//
//  Created by Temp on 21/01/22.
//

import Foundation

enum LoginErrors: String , Error{
    case userNameError = "Username not available"
    case passwordError = "Wrong Password"
    case obviousError = "password cannot be used as a password"
}
