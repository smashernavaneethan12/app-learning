
import Foundation

struct Credentials{
    var userName : String
    var password : String
    
    mutating func addUsername(userNameToBeAdded userName: String){
        self.userName = userName
    }
    
    mutating func addPassword(setYourPassword password: String){
        self.password = password
    }
}
