//
//  AllTrack.swift
//  MP3_Player
//
//  Created by Temp on 21/01/22.
//

import Foundation
class AllTrack{
    var clockTimer = Timer()
    private var remainingTime = 0
    private var songs:[Songs] = []
    private var songNumber = 0
    
    var totalSongs : Int{
        return songs.count
    }
    var songnumber : Int {
        get{
            return songNumber+1
        }
        set{
            songNumber = newValue-1
        }
    }
    
    
    func addNewSong(title: String,lengthInSeconds: Int,performedBy: [String],genre: Genres,movie: String,writtenBy: [String
    ],producedBy:[String],time: Int){
        let newSong = Songs(title: title, lengthInSeconds: lengthInSeconds, performedBy: performedBy, genre: genre, movie: movie, writtenBy: writtenBy, producedBy: producedBy,time: time)
        songs.append(newSong)
    }
    
    func playNext(){
        if songNumber+1<songs.count{
            songNumber += 1
            //print("Playing \(songs[songNumber].title)...")
            //            clockTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(playThisSong), userInfo: nil, repeats: true)
            playCurrentSong()
        }else{
            songNumber = 0
            //            clockTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(playThisSong), userInfo: nil, repeats: true)
            playCurrentSong()
            //print("Playing \(songs[songNumber].title)...")
        }
    }
    
    func playPrev(){
        if songNumber != 0{
            songNumber -= 1
            //print("Playing \(songs[songNumber].title)...")
            playCurrentSong()
            //            clockTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(playThisSong), userInfo: nil, repeats: true)
        }else{
            songNumber = songs.count-1
            //print("Playing \(songs[songNumber].title)...")
            playCurrentSong()
            //            clockTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(playThisSong), userInfo: nil, repeats: true)
        }
    }
    
    private func playThisSong(){
        while true{
            if remainingTime > 0{
                
                print("Playing \(songs[songNumber].title)...Remaining Time: \(remainingTime)sec")
                remainingTime -= 1
            }else{
                break
            }
        }
        //playNext()
    }
    
    func playCurrentSong(){
        if songs.count == 0{
            print("No Songs to Play :(",terminator: "\t")
            print("Add Some songs to play")
            exit(0)
        }else{
            
            remainingTime = songs[songNumber].time
            playThisSong()
            //            clockTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(playThisSong), userInfo: nil, repeats: true)
        }
    }
    
    func showDetails(){
        print("Title : \(songs[songNumber].title)")
        print("Genre : \(songs[songNumber].genre)")
        print("Movie : \(songs[songNumber].movie)")
        print("Produced by : \(songs[songNumber].producedBy)")
        print("Written by : \(songs[songNumber].writtenBy)")
        print("Performed by : \(songs[songNumber].performedBy)")
    }
    
    func showSongs(forAuthor author: String){
        for song in songs {
            if song.performedBy.contains(author){
                print(song.title)
            }
        }
    }
    
    func playSong(songName title: String)throws{
        var flag = 0
        for (index, song) in songs.enumerated(){
            if song.title == title{
                songNumber = index
                remainingTime = songs[songNumber].time
                playThisSong()
                flag = 1
            }
        }
        if flag == 0{
            throw PlayerError.songError
        }
    }
    
    func showSongNumber(){
        if songs.count == 0{
            print("No Songs Here :(")
        }else{
            print("Song Number : \(songNumber+1)")
        }
    }
    
    func showPlaylist(){
        for (index,song) in songs.enumerated() {
            print("\(index+1). \(song.title)")
        }
    }
    
    func pushSongs(forArtist artist: String) -> AllTrack{
        let newTrack = AllTrack()
        for song in songs {
            if song.performedBy.contains(artist){
                newTrack.songs.append(song)
            }
        }
        return newTrack
    }
    
    func pushSongs(forMovie movie: String) -> AllTrack{
        let newTrack = AllTrack()
        for song in songs {
            if song.movie == movie{
                newTrack.songs.append(song)
            }
        }
        return newTrack
    }
    
    func playSong(songNumber: Int){
        if songNumber > songs.count{
            print("No song Available for number: \(songNumber)")
        }else{
            self.songNumber = songNumber-1
            remainingTime = songs[songNumber].time
            playThisSong()
        }
    }
    
    func sortSongs(by: String){
        songnumber = 1
        if by.lowercased() == "alphabatically"{
            songs = songs.sorted(by: { (s1: Songs, s2: Songs)->Bool in
                s1.title < s2.title
            })
        }else if by.lowercased() == "reversed"{
            songs = songs.reversed()
        }else if by.lowercased() == "movie"{
            songs = songs.sorted(by: { (s1:Songs, s2:Songs)->Bool in
                if s1.movie == s2.movie {
                    return s1.title < s2.title
                }
                return s1.movie < s2.movie
            })
        }
    }
    
    func removeSong(name: String){
        var removeFlag = 0
        for (index,song) in songs.enumerated(){
            if song.title.lowercased() == name.lowercased(){
                songs.remove(at: index)
                removeFlag = 1
            }
            
        }
        if removeFlag == 0{
            print("No song available in this name :(")
        }
    }
    
    
}

