//
//  SigningIn.swift
//  MP3_Player
//
//  Created by Temp on 22/01/22.
//

import Foundation

struct SignIn{
    var register = Register()
    var flag = 0
    //    let newUserName = "Navaneethan G"
    //    let userPassword = "1234"
    mutating func registerUser(newUserName: String, userPassword: String)throws{
        do {
            try register.signIn(userName: newUserName, password: userPassword)
            print("Logged Successfully!")
            flag = 1
        } catch LoginErrors.userNameError {
            print(LoginErrors.userNameError.rawValue)
        }catch LoginErrors.passwordError{
            print(LoginErrors.passwordError.rawValue)
            exit(0)
        }
        if flag == 0{
            print("Adding new User... :)")
            do{
                try register.signUp(userName: newUserName, password: userPassword)
                print("User Added Successfully!")
            }
            catch LoginErrors.obviousError{
                print("User not Added :(")
                print(LoginErrors.obviousError.rawValue)
                exit(1)
            }
        }
    }
}
