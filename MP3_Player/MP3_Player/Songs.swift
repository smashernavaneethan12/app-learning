
import Foundation

struct Songs{
    var title: String
    var lengthInSeconds : Int
    var performedBy: [String]
    var genre: Genres
    var movie: String
    var writtenBy: [String]
    var producedBy: [String]
    var time : Int
}
